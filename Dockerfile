FROM beagle/debian-build:bullseye as sphinx-build-env
ARG TARGETARCH
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get install -y apt-utils \
    && rm -rf /var/lib/apt/lists/*
RUN apt-get update \
    && apt-get install -y locales \
	&& localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 \
    && rm -rf /var/lib/apt/lists/*
ENV LANG en_US.utf8
RUN apt-get update \
    && apt-get install -y \
        make rsync git wget \
        doxygen graphviz librsvg2-bin texlive-latex-base texlive-latex-extra latexmk texlive-fonts-recommended \
        python3 python3-pip python3-pil \
        imagemagick-6.q16 librsvg2-bin webp \
        texlive-full texlive-latex-extra texlive-fonts-extra \
        fonts-freefont-otf fonts-dejavu fonts-dejavu-extra fonts-freefont-ttf \
    && rm -rf /var/lib/apt/lists/* 
RUN python3 -m pip install --upgrade pip
RUN pip install -U sphinx==5.3.0 sphinx-rtd-theme sphinx_design sphinx-tabs sphinxcontrib.svg2pdfconverter sphinx-reredirects sphinx_copybutton
RUN if [ "$TARGETARCH" = "arm64" ]; then \
        wget https://github.com/pdfcpu/pdfcpu/releases/download/v0.4.0/pdfcpu_0.4.0_Linux_arm64.tar.xz \
        && tar xf pdfcpu_0.4.0_Linux_arm64.tar.xz && mv pdfcpu_0.4.0_Linux_arm64/pdfcpu /usr/local/bin/ && /usr/local/bin/pdfcpu version ; \
    elif [ "$TARGETARCH" = "amd64" ]; then \
        wget https://github.com/pdfcpu/pdfcpu/releases/download/v0.4.0/pdfcpu_0.4.0_Linux_x86_64.tar.xz \
        && tar xf pdfcpu_0.4.0_Linux_x86_64.tar.xz && mv pdfcpu_0.4.0_Linux_x86_64/pdfcpu /usr/local/bin/ && /usr/local/bin/pdfcpu version ; \
    fi \
    && rm -rf pdfcpu_0.4.0_Linux_*
RUN pip install -U sphinxcontrib-images
RUN pip install -U breathe exhale
RUN pip install -U graphviz
RUN pip install -U sphinxcontrib-youtube
